{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}

module AlwaysSucceedsValidator where

import Plutus.V2.Ledger.Api (BuiltinData, Validator, mkValidatorScript)
import PlutusTx (compile)

{-# INLINEABLE myValidator #-}
myValidator :: BuiltinData -> BuiltinData -> BuiltinData -> ()
myValidator _ _ _ = ()

validator :: Validator
validator = mkValidatorScript $$(compile [||myValidator||])